#include "MyAnalysis/MyClass.h" 

// this is needed to distribute the algorithm to the workers
ClassImp(MyClass)



// 


// #include "xAODBTagging/BTagging.h"
// #include "xAODEventInfo/EventInfo.h"

// #include "CxAODTools/DummyEvtSelection.h"
// #include "CxAODTools/TriggerTool.h"
// #include "TFile.h"
// #include "TLorentzVector.h"
// #include "TSystem.h"

// //Regular expression include
// #include <regex>

// #include <SampleHandler/DiskOutputXRD.h>

// #include "xAODRootAccess/tools/TFileAccessTracer.h"


void MyClass::SetAllPointers_ToNull()
{
  
  m_eventInfo        = 0 ;
  m_mmc              = 0 ;
  m_electrons        = 0 ;
  m_forwardElectrons = 0 ;
  m_photons          = 0 ;
  m_muons            = 0 ;
  m_taus             = 0 ;
  m_ditaus           = 0 ;
  m_jets             = 0 ;
  m_fatJets          = 0 ;
  m_fatJetsAlt       = 0 ;
  m_trackJets        = 0 ;
  m_subJets          = 0 ;
  m_metCont          = 0 ;
  m_met              = 0 ;
  m_met_soft         = 0 ;
  m_mptCont          = 0 ;
  m_truthCont        = 0 ;
  m_mpt              = 0 ;
  m_truthMET         = 0 ;
  m_truthEvent       = 0 ;
  m_truthParts       = 0 ;
  m_truthMuons       = 0 ;
  m_truthTaus        = 0 ;
  m_truthElectrons   = 0 ;
  m_truthNeutrinos   = 0 ;
  m_truthWZJets      = 0 ;
  m_truthWZFatJets   = 0 ;
}

MyClass::MyClass()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  SetAllPointers_ToNull() ; 
}

EL::StatusCode MyClass::setupJob(EL::Job &job) {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  if (m_debug) Info("setupJob()", "Setting up job.");

  job.useXAOD();

  // check if ConfigStore is set
  // if (!m_config) {
  //   Error("setupJob()",
  //         "ConfigStore not setup! Remember to set it via setConfig()!");
    //return EL::StatusCode::FAILURE;
  //}

  // bool writeMVATree = false;
  // bool writeEasyTree = false;
  // string m_eosPATH = "none";
  // m_config->getif<std::string>("eosFolderUser", m_eosPATH);

  // EL::OutputStream out("MVATree");
  // m_config->getif<bool>("writeMVATree", writeMVATree);
  // m_config->getif<bool>("writeEasyTree", writeEasyTree);

  // if (writeMVATree || writeEasyTree) {
  //   if (m_eosPATH != "none") {
  //     out.output(new SH::DiskOutputXRD("root://eosuser.cern.ch/" + m_eosPATH));
  //   }
  //   job.outputAdd(out);
  // }


  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init("MyxAODAnalysis").ignore();  // call before opening first file

  return EL::StatusCode::SUCCESS;
}  // setupJob

EL::StatusCode MyClass::histInitialize() {
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  Info("histInitialize()", "Initializing histograms.");

  return EL::StatusCode::SUCCESS;
}  // histInitialize

EL::StatusCode MyClass::fileExecute() {
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  // std::cout << "fileExecute Input file is " << wk()->inputFile()->GetName() << std::endl;
  //QG Tagging Calibration Truth Match Files need to switch

  return EL::StatusCode::SUCCESS;
}  // fileExecute



EL::StatusCode MyClass::changeInput(bool /*firstFile*/) {
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

  // It seems PROOF-lite needs the file initialization here

  TFile *inputfile = wk()->inputFile();
  TString filename(inputfile->GetName());

  Info("changeInput()", "Processing file '%s'", filename.Data());

  return EL::StatusCode::SUCCESS;
}  // changeInput

EL::StatusCode MyClass::initialize() {
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  Info("initialize()", "Dumping config...");

  // EL_CHECK("initialize()", initializeCxAODTag());
  // EL_CHECK("initialize()", initializeEvent());
  // EL_CHECK("initialize()", initializeReader());
  // EL_CHECK("initialize()", initializeIsMC());
  // EL_CHECK("initialize()", initializeVariations());
  // EL_CHECK("initialize()", initializeSelection());
  // EL_CHECK("initialize()", initializeTools());
  // EL_CHECK("initialize()", initializeSumOfWeights());

  m_event = wk()->xaodEvent();

  return EL::StatusCode::SUCCESS;
}  // initialize


EL::StatusCode MyClass::execute() {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  SetAllPointers_ToNull() ; 
  m_nevent_processed += 1 ; 

  ANA_CHECK (evtStore()->retrieve(m_eventInfo , "EventInfo___Nominal")); 
  ANA_CHECK (evtStore()->retrieve(m_jets      , "AntiKt4EMTopoJets___Nominal")); 
  ANA_CHECK (evtStore()->retrieve(m_fatJets   , "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets___Nominal")) ; 

  std::cout << std::endl ; 
  std::cout << "iEvent       = " << m_nevent_processed << std::endl ;
  std::cout << "runNumber    = " << m_eventInfo->runNumber()   << std::endl ; 
  std::cout << "eventNumber  = " << m_eventInfo->eventNumber() << std::endl ;


  process_event() ; 


  return EL::StatusCode::SUCCESS;
}  // execute






void MyClass::check_ghostrackjets(const xAOD::Jet & fatjet )
{
  // AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets___NominalAuxDyn.GhostVR30Rmax4Rmin02TrackJet

  typedef std::vector<ElementLink<DataVector<xAOD::IParticle>>> AuxDataType;
  static const SG::AuxElement::ConstAccessor<AuxDataType> acc( "GhostVR30Rmax4Rmin02TrackJet" );

  if( ! acc.isAvailable( fatjet ) ) 
  {
  //   //return nullptr;

    std::cout << "    Error could not access Ghost Track Jets" << std::endl ; 
    std::cout << "    fatjet.pt()  = " << fatjet.pt()                << std::endl ; 
    std::cout << "    fatjet.eta() = " << fatjet.eta()               << std::endl ; 
    // exit function
    return ; 
  }

  else 
  {
    std::cout << "    OK for Ghost Track Jets" << std::endl ; 
    const AuxDataType& theauxdata = acc( fatjet );

    bool print_pt = true ; 

    if (print_pt)
    {
      for (unsigned int iPart = 0 ; iPart < theauxdata.size() ; iPart++)
      {
        const ElementLink<DataVector<xAOD::IParticle>> link = theauxdata[iPart] ; 
        if (link.isValid())
        {
          const xAOD::Jet* GRTrackjet = (xAOD::Jet*) *link ;
          std::cout << "       GRTrackjet->pt() = " <<  GRTrackjet->pt() << std::endl ;  
        }
        else 
        {
          std::cout << "       Elementlink is not valid" << std::endl ;   
        }
      }
    }
  }
}

void MyClass::process_event() 
{ 
  //loop over jets 

  // std::cout << "Printing pT of normal jets" << std::endl ; 
  // for (const xAOD::Jet *jet : *m_jets)
  // {
  //   std::cout << jet->pt() << std::endl ; 
  // } 

  // std::cout << "Printing pT of fat jets" << std::endl ; 
  // for (const xAOD::Jet * fatjet : *m_fatJets)
  // {
  //   std::cout << fatjet->pt() << std::endl ; 
  // }
 
  std::cout << "NFatJets = " << m_fatJets->size() << std::endl ; 
  unsigned int Icounterfatjet = 0 ; 
  for (const xAOD::Jet * fatjet : *m_fatJets)
  {
    //fatjet 
    std::cout << "  Icounterfatjet = " << Icounterfatjet << std::endl ; 
    Icounterfatjet++ ; 

    check_ghostrackjets(*fatjet) ; 

    static const SG::AuxElement::ConstAccessor<double> acc1( "myvar1" );
    static const SG::AuxElement::ConstAccessor<double> acc2( "myvar2" );

    // check can retrieve myvar1 
    if (!acc1.isAvailable(*fatjet))
    {
      std::cout << "    accessor acc1 is not available" << std::endl ; 
    }
    else 
    {
      double myvar1 = acc1(*fatjet) ; 
      std::cout << "    myvar1 = " << myvar1 << std::endl ; 
    }

    // check can retrieve myvar2 
    if (!acc2.isAvailable(*fatjet))
    {
      std::cout << "    accessor acc2 is not available" << std::endl ; 
    }
    else 
    {
      double myvar2 = acc2(*fatjet) ; 
      std::cout << "    myvar2 = " << myvar2 << std::endl ; 
    }
  }
}

EL::StatusCode MyClass::postExecute() {
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.

  return EL::StatusCode::SUCCESS;
}  // postExecute

EL::StatusCode MyClass::finalize() {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  Info("finalize()", "Finalizing job.");


  return EL::StatusCode::SUCCESS;
}  // finalize

EL::StatusCode MyClass::histFinalize() {
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  Info("histFinalize()", "Finalizing histograms.");
  return EL::StatusCode::SUCCESS;
}  // histFinalize
