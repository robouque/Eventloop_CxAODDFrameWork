#ifndef MYCLASS_H
#define MYCLASS_H


#include <algorithm>
#include <utility>
#include <vector>

#include <EventLoop/Algorithm.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Job.h>
#include <EventLoop/OutputStream.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <EventLoop/IWorker.h>


// Infrastructure include(s):
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODTruth/TruthEventContainer.h> 
#include <xAODEgamma/ElectronContainer.h> 
#include <xAODMuon/MuonContainer.h> 
#include <xAODEgamma/PhotonContainer.h> 
#include <xAODMissingET/MissingETContainer.h> 
#include <xAODJet/JetContainer.h>
#include<xAODTau/TauJetContainer.h> 
#include<xAODTau/DiTauJetContainer.h> 

#include <xAODCore/AuxContainerBase.h>


#ifndef __MAKECINT__
// other includes here from other class 
#include <TH1D.h>
#include <TH1F.h>


// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop
// Access the Data Through xAOD EDM 

#endif  // not __MAKECINT__



// important comment //! are important 
//If there are any std::vector variables, make sure you add a //! in the end to protect them from CINT (otherwise you will experience random crashes), e.g.: 
// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop

class MyClass : public EL::Algorithm 
{
  public: 
    MyClass(); 
    ~MyClass() {}

   // these are the functions from Algorithm
  private: 
    bool m_debug                    = false ; 
    unsigned int m_nevent_processed = 0 ; 

    void check_ghostrackjets(const xAOD::Jet & /* fatjet */ ) ; 
    void process_event() ; 
    
    // see example CxAODFramework
    // https://gitlab.cern.ch/CxAODFramework/CxAODReader/-/blob/master/CxAODReader/AnalysisReader.h
    // https://gitlab.cern.ch/CxAODFramework/CxAODReader/-/blob/master/Root/AnalysisReader.cxx
    
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob(EL::Job &job) override;
    virtual EL::StatusCode fileExecute() override;
    virtual EL::StatusCode histInitialize() override;
    virtual EL::StatusCode changeInput(bool firstFile) override;
    virtual EL::StatusCode initialize() override;
    virtual EL::StatusCode execute() override;
    virtual EL::StatusCode postExecute() override;
    virtual EL::StatusCode finalize() override;
    virtual EL::StatusCode histFinalize() override;

    void SetAllPointers_ToNull() ; 

    
    const xAOD::EventInfo              *m_eventInfo          ;// !
    const xAOD::EventInfo              *m_mmc                ;// !
    const xAOD::ElectronContainer      *m_electrons          ;// !
    const xAOD::ElectronContainer      *m_forwardElectrons   ;// !
    const xAOD::PhotonContainer        *m_photons            ;// !
    const xAOD::MuonContainer          *m_muons              ;// !
    const xAOD::TauJetContainer        *m_taus               ;// !
    const xAOD::DiTauJetContainer      *m_ditaus             ;// !
    const xAOD::JetContainer           *m_jets               ;// !
    const xAOD::JetContainer           *m_fatJets            ;// !
    const xAOD::JetContainer           *m_fatJetsAlt         ;// !
    const xAOD::JetContainer           *m_trackJets          ;// !
    const xAOD::JetContainer           *m_subJets            ;// !
    const xAOD::MissingETContainer     *m_metCont            ;// !
    const xAOD::MissingET              *m_met                ;// !
    const xAOD::MissingET              *m_met_soft           ;// !
    const xAOD::MissingETContainer     *m_mptCont            ;// !
    const xAOD::MissingETContainer     *m_truthCont          ;// !
    const xAOD::MissingET              *m_mpt                ;// !
    const xAOD::MissingET              *m_truthMET           ;// !
    const xAOD::TruthEventContainer    *m_truthEvent         ;// !
    const xAOD::TruthParticleContainer *m_truthParts         ;// !
    const xAOD::TruthParticleContainer *m_truthMuons         ;// !
    const xAOD::TruthParticleContainer *m_truthTaus          ;// !
    const xAOD::TruthParticleContainer *m_truthElectrons     ;// !
    const xAOD::TruthParticleContainer *m_truthNeutrinos     ;// !
    const xAOD::JetContainer           *m_truthWZJets        ;// !
    const xAOD::JetContainer           *m_truthWZFatJets     ;// !


    /// description: the event we are reading from
    xAOD::TEvent *m_event; //!
    xAOD::TStore *m_store; //! 

    // this is needed to distribute the algorithm to the workers
    ClassDefOverride(MyClass, 1);
}; 

#endif