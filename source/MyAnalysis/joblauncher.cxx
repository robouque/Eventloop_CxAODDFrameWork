
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"


#include "EventLoop/Job.h"
#include "SampleHandler/DiskListEOS.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"

#include "EventLoop/CondorDriver.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/Driver.h"

#include "SampleHandler/Sample.h"


#include <stdlib.h>
#include <fstream>
#include <vector>

#include <TFile.h>
#include <TSystem.h>

#include "MyAnalysis/MyClass.h" 

//see https://twiki.cern.ch/twiki/bin/view/Sandbox/RootCoreTutorialCopy

int main(int argc, char ** argv) 
{ 
  // cd ../run ; joblauncher test_dir_v1 

  if (argc !=2 )
  {
    std::cout << "Error in joblauncher, please provide a submitDir" << std::endl ; 
    std::exit(1) ; 
  }

  std::string submitDir = argv[1] ; 




  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create the sample handler
  SH::SampleHandler sampleHandler;

  std::string 
    sample_name = "" ,
    sample_dir  = "" ; 
  
  // r32-15 
  //sample_dir = "/afs/cern.ch/user/b/bouquet/work/private/VHbb_from_32_15/run/Maker_1L_e_VHbb_none_PF_0_TCC_0_data18_13TeV.00357500.physics_Main.deriv.DAOD_HIGG5D2.f958_m2010_p3718_none/data-CxAOD/" ; 

  // r33-01
  sample_dir = "/afs/cern.ch/user/b/bouquet/work/private/VHbb_from_r33-01/run/Maker_1L_e_VHbb_none_PF_0_TCC_0_data18_13TeV.00357500.physics_Main.deriv.DAOD_HIGG5D2.f958_m2010_p3718_none/data-CxAOD/" ; 
  sample_name = "data" ; 

  //filename = "/eos/user/b/bouquet/data18_13TeV.00357500.physics_Main.deriv.DAOD_HIGG5D2.f958_m2010_p3718/DAOD_HIGG5D2.16464363._000092.pool.root.1"

  SH::DiskListLocal list = SH::DiskListLocal(sample_dir);
 
  SH::ScanDir()
    .samplePattern("*pool.root.1*")
    .sampleName(sample_name)
    .scan(sampleHandler, list);
      
  SH::Sample* sample_ptr = sampleHandler.get(sample_name);
  // sample_ptr->setMetaString("SampleID", sample_name);
  //sample_ptr->meta()->setString("SampleID", sample_name);
  int nsampleFiles = sample_ptr->numFiles();

  std::cout << "Sample name " << sample_name << " with nfiles : " << nsampleFiles << std::endl;
  
  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sampleHandler.setMetaString("nc_tree", "CollectionTree");

  // Print what we found:
  std::cout << "Printing sample handler contents:" << std::endl;
  sampleHandler.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler(sampleHandler);


  EL::OutputStream outfile ("outputFile", "xAOD");
  job.outputAdd (outfile);

  
  //int maxEvent = 10 ; 
  // std::cout << "Will only run over the " << maxEvent << " first events" << std::endl ; 
  // job.options()->setDouble (EL::Job::optMaxEvents, maxEvent);


  // remove submit dir before running the job
  // job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);

  // create algorithm, set job options, maniplate members and add our analysis
  // to the job:
  MyClass         * algorithm = new MyClass() ; 
  EL::DirectDriver* eldriver  = new EL::DirectDriver;
 
  // add algorithm to job
  job.algsAdd(algorithm);

  // job.options()->setDouble(EL::Job::optFilesPerWorker, nFilesPerJob);
  // job.options()->setString(EL::Job::optSubmitFlags, submit_flags);
  eldriver->submit(job, submitDir);

  return 0;
}
