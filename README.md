To use the code 


Edit [source/joblauncher.cxx] to run over the CxAOD file(s) you want 
It will launch a direct job  

```
mkdir build 
mkdir run 
source setup.sh 
cd build 
cmake ../source 
cmake --build .
source x86_64-centos7-gcc8-opt/setup.sh 
cd ../run
joblauncher NameOFOutputDirectory 
```

When opening a new terminal and if you don't need to compile again the code you can just `source setup.sh && cd run`
