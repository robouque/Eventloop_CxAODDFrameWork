
VERSION="21.2.135"
echo "Version AnalysisBase = $VERSION"

cd build
setupATLAS
lsetup git
asetup AnalysisBase,$VERSION,here
source x*/setup.sh
cd ..
